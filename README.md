# SASS Compile
Provides functionality for convert SASS file into CSS files using admin
dashboard and drush command.

## Requirements

This module requires no modules outside of Drupal core.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Drush Command

```drush sass:compile path/to/scss_file/scss path/to/css_file/css```

## Admin Configuration URL

1. Enable the module at Administration > Extend.
1. Visit `/admin/config/development/sass-compile` for configurations of module.

## Maintainers

- Poornima Koganti - [kpoornima](https://www.drupal.org/u/kpoornima)
- Ram Reddy Kancherla - [ramreddy.kancherla](https://www.drupal.org/u/ramreddykancherla)
