<?php

namespace Drupal\sass_compile;

use ScssPhp\ScssPhp\Formatter\Expanded;
use Drupal\Core\File\FileSystemInterface;
use ScssPhp\ScssPhp\Compiler;
use Psr\Log\LoggerInterface;

/**
 * Class use to Compile the file scss and css .
 */
class SassCompileService implements SassCompileServiceInterface {

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a new SassCompileService.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   */
  public function __construct(LoggerInterface $logger) {
    $this->logger = $logger;
  }

  /**
   * Constructs a new SassCompileService object.
   *
   * @param null $source
   *   SASS files location.
   * @param null $destination
   *   CSS files save location.
   *
   * @return bool|string
   *   TRUE on success, FALSE on failure.
   *
   * @throws \Exception
   */
  public function compileScssFiles($source = NULL, $destination = NULL) {
    // If SASS files source and css file save destination is empty,
    // then skip further execution.
    if (!$source || !$destination) {
      $this->logger->info('SASS compile failed. Source and destination paths required.');
      return FALSE;
    }
    $source_sass = realpath(".") . base_path() . $source;
    $destination_css = realpath(".") . base_path() . $destination;
    $dir = opendir($source_sass);
    $filetype = 'scss';
    while ($file = readdir($dir)) {
      if (pathinfo($file, PATHINFO_EXTENSION) == $filetype) {
        try {
          // Create an instance of the Sass Compiler class.
          $scss = new Compiler();

          // Add the expanded formatter to produce a readable format of CSS.
          $scss->setFormatter(new Expanded());
          // Path to the folder of the sass files.
          $sassFilesPath = $source_sass . '/' . $file;

          // Path to the folder of css files.
          $newCssFile = str_replace('scss', 'css', $destination_css . '/' . $file);

          $css = $scss->compile(
            file_get_contents(
              $sassFilesPath
            )
          );

          $cssfile = \Drupal::service('file_system')->saveData($css, $newCssFile, FileSystemInterface::EXISTS_REPLACE);
          if ($cssfile) {
            $this->logger->info('Successfully created css file at @file ', [
              '@file' => $newCssFile,
            ]);
          }
          else {
            $this->logger->error('CSS file creation failed file @file ', [
              '@file' => $newCssFile,
            ]);
          }
        }
        catch (\Exception $e) {
          $this->logger->error($e);
        }

      }
    }
    closedir($dir);
  }

}
