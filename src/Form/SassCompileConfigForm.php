<?php

namespace Drupal\sass_compile\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sass_compile\SassCompileService;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class for configuration settings.
 *
 * @package Drupal\sass_compile\Form
 */
class SassCompileConfigForm extends ConfigFormBase {

  /**
   * Saas Compile.
   *
   * @var Drupal\sass_compile\SassCompileService
   */
  protected $sassCompile;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs an object.
   *
   * @param Drupal\sass_compile\SassCompileService $sassCompile
   *   The sass compile.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(SassCompileService $sassCompile, MessengerInterface $messenger) {
    $this->sassCompile = $sassCompile;
    $this->messenger = $messenger;
  }

  /**
   * Create method.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('sass_compile.sass_compile'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'sass_compile.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sass_compile_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('sass_compile.settings');
    $form['sass_files_folder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('SASS files location'),
      '#description' => $this->t('Sass files folder path. Ex. themes/themeName/sass'),
      '#default_value' => $config->get('sass_files_folder'),
    ];
    $form['css_files_folder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CSS files save location'),
      '#description' => $this->t('CSS files save folder path. Ex. themes/themeName/css'),
      '#default_value' => $config->get('css_files_folder'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config('sass_compile.settings')
      ->set('sass_files_folder', $form_state->getValue('sass_files_folder'))
      ->set('css_files_folder', $form_state->getValue('css_files_folder'))
      ->save();
    if ($form_state->getValue('sass_files_folder') && $form_state->getValue('css_files_folder')) {
      $source = $form_state->getValue('sass_files_folder');
      $destination = $form_state->getValue('css_files_folder');
      $this->sassCompile->compileScssFiles($source, $destination);
      $this->messenger->addMessage("Compilation completed. Please refer log for more information.");
    }
  }

}
