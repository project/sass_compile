<?php

namespace Drupal\sass_compile\Commands;

use Drush\Commands\DrushCommands;
use Drupal\sass_compile\SassCompileService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A Drush command file.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 */
class SassCompileCommands extends DrushCommands {

  /**
   * Saas Compile.
   *
   * @var Drupal\sass_compile\SassCompileService
   */
  protected $sassCompile;

  /**
   * Constructs an object.
   *
   * @param Drupal\sass_compile\SassCompileService $sassCompile
   *   The sass compile.
   */
  public function __construct(SassCompileService $sassCompile) {
    $this->sassCompile = $sassCompile;
  }

  /**
   * Create method.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('sass_compile.sass_compile')
    );
  }

  /**
   * Compile sass files based on given color code.
   *
   * @param string $source
   *   Argument provided to the drush command.
   * @param string $destination
   *   Argument provided to the drush command.
   * @param string $options
   *   Argument provided to the drush command.
   *
   * @command sass:compile
   * @aliases compile:sass
   * @options arr An option that takes multiple values.
   * @usage sass:compile themes/themeName/scss themes/themeName/css
   */
  public function sassCompile($source, $destination, $options = []) {

    $this->sassCompile->compileScssFiles($source, $destination);
    return $this->output()->writeln('Compilation completed. Please refer log for more information.');
  }

}
